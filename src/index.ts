import * as gitlabEnv from 'gitlab-ci-env';

interface gitlabPipeline {
    id: number;
    source: string;
}

interface gitlabProject {
    id: number;
    name: string;
    namespace: string;
}

interface gitlabEnv {
    pipeline: gitlabPipeline;
    project: gitlabProject;
}

const pipeline: gitlabPipeline = {
    id: gitlabEnv.ci.pipeline.id,
    source: gitlabEnv.ci.pipeline.source
};

const project: gitlabProject = {
    id: gitlabEnv.ci.project.id,
    name: gitlabEnv.ci.project.name,
    namespace: gitlabEnv.ci.project.namespace
};

const customEnv: gitlabEnv = {
    pipeline,
    project
};

export default customEnv;
